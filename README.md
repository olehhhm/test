* Створення користувача: /user/create?login=login&password=1234&password_confirmation=1234
* Логін користувача і отримання: login_token для подальшої роботи з закритими методами: /user/login?login=login&password=1234 
* Добавлення валюти в список відтежуваних: /currency_tracker/subscribe?token=6271a92ae91cfd6c237d2cdd0bf5ded9&from=UAH&to=EUR
* Витягнування всіх відстежуваних валют з їхніми курсами: /currency_tracker/list?token=6271a92ae91cfd6c237d2cdd0bf5ded9
