class ApplicationController < ActionController::API
  def check_token
    return render :json => {error_code: 'DEF1', error_desc: 'Access denied'} unless params[:token].present?
    if user = User.find_by_token(params[:token])
      @current_user = user
    else
      render :json => {error_code: 'DEF1', error_desc: 'Access denied'}
    end
  end
end
