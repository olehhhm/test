class CurrencyTrackerController < ApplicationController
  before_action :check_token

  def subscribe
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} if !params[:from].present? || !(params[:from].is_a?(String) && params[:from].size == 3)
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} if !params[:to].present? || !(params[:to].is_a?(String) && params[:to].size == 3)
    unless UserCurrency.where('user_id = :user_id AND from_currency = :from AND to_currency = :to', {user_id: @current_user.id, from: params[:from], to:params[:to]}).exists?
      if UserCurrency.create({user_id: @current_user.id, from_currency: params[:from], to_currency:params[:to]})
        render :json => {status: 'OK'}
      else
        render :json => {error_code: 'CTS3', error_desc: (user.errors.messages.first[1] rescue nil)}
      end
    else
      render :json => {error_code: 'CTS2', error_desc: 'Already exists'}
    end
  end

  def list
    user_currency = @current_user.user_currency
    return render :json => {error_code: 'CTL1', error_desc: 'Not found currencies list'} unless user_currency.present?
    result = user_currency.map{|one_record|
      [
        "#{one_record.from_currency}_#{one_record.to_currency}",
        CurrencyExchanger.get(one_record.from_currency, one_record.to_currency, 1) || 0
      ]
    }.to_h
    render :json => result
  end
end
