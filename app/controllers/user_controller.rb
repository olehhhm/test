class UserController < ApplicationController
  def create
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} unless params[:login].present?
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} unless params[:password].present?
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} unless params[:password_confirmation].present?

    if User.where('login = :login', login: params[:login]).exists?
      render :json => {error_code: 'UCC2', error_desc: 'User already exists'}
    else
      user =  User.new(login: params[:login], password: params[:password], password_confirmation: params[:password_confirmation])
      if user.save
        render :json => {status: 'OK'}
      else
        render :json => {error_code: 'UCC3', error_desc: (user.errors.messages.first[1] rescue nil)}
      end
    end
  end

  def login
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} unless params[:login].present?
    return render :json => {error_code: 'DEF2', error_desc: 'Wrong params'} unless params[:password].present?

    if user = User.where('login = :login', login: params[:login]).first.try(:authenticate, params[:password])
      user.login_token = SecureRandom.hex
      user.token_expire = Time.now + 12.hours
      user.save
      render :json => {login: user.login, login_token: user.login_token}
    else
      render :json => {error_code: 'UCL2', error_desc: 'Wrong login or password'}
    end
  end
end
# bin/rails generate migration CreateUser
