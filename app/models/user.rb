class User < ApplicationRecord
  has_many :user_currency, dependent: :destroy
  has_secure_password

  def self.find_by_token token
    User.where('login_token = :token AND token_expire > :expire', {token: token, expire: Time.now}).first
  end
end
