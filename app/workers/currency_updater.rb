require 'sidekiq-scheduler'

class CurrencyUpdater
  include Sidekiq::Worker

  def perform
    UserCurrency.select('from_currency, to_currency').group('from_currency, to_currency').all.each{|record|
      begin
        CurrencyExchanger.get(record.from_currency, record.to_currency, 1, true)
      rescue => e
        # log
      end
    }
  end
end
