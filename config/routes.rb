Rails.application.routes.draw do
  get 'user/login'
  get 'user/create'
  get 'currency_tracker/subscribe'
  get 'currency_tracker/list'
end
