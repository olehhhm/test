class CreateUserCurrency < ActiveRecord::Migration[5.2]
  def change
    create_table :user_currencies do |t|
      t.string :user_id
      t.string :from_currency
      t.string :to_currency
      t.timestamps
    end
  end
end
