require 'net/http'
class CurrencyExchanger
  def self.get from_currency, to_currency, value, force = false
    rate = nil
    if force || !rate = Rails.cache.fetch("currency_exchanger_rates_#{from_currency}_#{to_currency}")
      config = YAML.load_file("#{Rails.root.to_s}/config/project.yml")[Rails.env]
      uri = URI('https://www.amdoren.com/api/currency.php')
      params = {api_key: config['currency_api_key'], from: from_currency, to: to_currency, amount: value.to_i}
      uri.query = URI.encode_www_form(params)

      res = Net::HTTP.get_response(uri)
      if res.is_a?(Net::HTTPSuccess)
        begin
          d_result = ActiveSupport::JSON.decode(res.body)
          if d_result.try(:[], 'amount').to_f > 0
            rate = d_result.try(:[], 'amount').to_f
            Rails.cache.fetch("currency_exchanger_rates_#{from_currency}_#{to_currency}", expires_in: 30.minutes){rate}
          end
        rescue => e
          # log
        end
      else
        # log
      end
    end
    rate
  end
end
